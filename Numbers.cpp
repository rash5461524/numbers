﻿

#include <iostream>
using namespace std;

void displayOddOrNotNumbers(int maxNum, bool isOdd) {
    int i = !isOdd;
    for (; i <= maxNum; i+=2) {
        cout << i << "\n";
    }
}

int main()
{
    displayOddOrNotNumbers(10, true);
}
